from django.contrib.auth import authenticate, get_user_model
from rest_framework import serializers
from . import models


class SignupSerializer(serializers.Serializer):
    """
    Don't require email to be unique so visitor can signup multiple times,
    if misplace verification email.  Handle in view.
    """
    email = serializers.EmailField(max_length=255)
    password = serializers.CharField(max_length=128)
    full_name = serializers.CharField(max_length=30)
    # last_name = serializers.CharField(max_length=30, default='',
    #     required=False)
    is_cook = serializers.BooleanField(default=False)
    phoneNumber = serializers.IntegerField(default=False)
    location = serializers.CharField(max_length=40, default='')
    longitude = serializers.FloatField(default=False)
    latitude = serializers.FloatField(default=False)


class LoginSerializer(serializers.Serializer):
    email = serializers.EmailField(max_length=255)
    password = serializers.CharField(max_length=128)


class PasswordResetSerializer(serializers.Serializer):
    email = serializers.EmailField(max_length=255)


class PasswordResetVerifiedSerializer(serializers.Serializer):
    code = serializers.CharField(max_length=40)
    password = serializers.CharField(max_length=128)


class PasswordChangeSerializer(serializers.Serializer):
    password = serializers.CharField(max_length=128)


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = ('id', 'email', 'full_name')
