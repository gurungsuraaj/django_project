from django.conf.urls import *
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

admin.autodiscover()




urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/accounts/', include('authemail.urls')),
    url(r'^api/models/',include('accounts.urls')),
    url(r'^signup/verify/',include('authemail.urls'))

]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
