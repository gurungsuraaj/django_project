from __future__ import absolute_import
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import viewsets
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, JsonResponse
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.generics import UpdateAPIView
from rest_framework import status
from django.contrib.auth import authenticate, get_user_model
from decimal import Decimal
from math import sin, cos, sqrt, atan2, radians, asin

from rest_framework.decorators import api_view

from .serializers import MyUserSerializer
from . import serializers
from . import models
from .models import Product, Cart, MyUser, Order

from authemail.serializers import UserSerializer

from rest_framework import filters


class MyUserMe(APIView):
    #permission_classes = (IsAuthenticated,)
    serializer_class = MyUserSerializer

    def get(self, request, pk, format=None):
        user = get_user_model().objects.get(pk=pk)
        queryset = Product.objects.filter(owner=pk, category='BreakFast')
        serializer = serializers.ProductUserSerializer(queryset, many=True)
        # print(serializer.data)
        queryset1 = Product.objects.filter(owner=pk, category='Lunch')
        serializer1 = serializers.ProductUserSerializer(queryset1, many=True)
        queryset2 = Product.objects.filter(owner=pk, category='Dinner')
        serializer2 = serializers.ProductUserSerializer(queryset2, many=True)
        queryset3 = Product.objects.filter(owner=pk, category='Snacks')
        serializer3 = serializers.ProductUserSerializer(queryset3, many=True)

        return Response({
            "user": self.serializer_class(user).data,
            "user_product": {
                "breakfast": serializer.data,
                "lunch": serializer1.data,
                "dinner": serializer2.data,
                "snacks": serializer3.data
            }
        })


class HomeViewSet(APIView):

    # serializer_class = serializers.ProductsSerializer
    # queryset = models.Product.objects.all()
    # filter_backends = (filters.SearchFilter,)
    #  search_fields = ('name','rating')

    permission_classes = (IsAuthenticated, )

    def get(self, request):
        # serializer_class = serializers.ProductsSerializer
        queryset = Product.objects.all()[:5]
        queryset1 = Product.objects.all().order_by("-rating")[:5]
        # print(queryset1)
        serializer = serializers.ProductUserSerializer(queryset, many=True)
        serializer1 = serializers.ProductUserSerializer(queryset1, many=True)
        # print(serializer.data)
        return Response({
            "top_rated": serializer.data,
            "recommended": serializer1.data,
            "recently_added": serializer.data
        })


class RatingViewSet(viewsets.ModelViewSet):

    serializer_class = serializers.RatingSerializer
    queryset = models.ProductsRating.objects.all()


# class AddressViewSet(viewsets.ModelViewSet):

#     serializer_class = serializers.AddressSerializer
#     queryset= models.Address.objects.all()


class CartViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.CartSerializer
    queryset = models.Cart.objects.filter(ordered=False)
    permission_classes = (IsAuthenticated, )

    def get_queryset(self):
        print("#################", self.request.user)
        return self.queryset.filter(user=self.request.user)

    def perform_create(self, serializer):
        request_item = self.request.data['items']
        print(request_item)
        cart = self.queryset.filter(user=self.request.user, items=request_item)

        serializer.save(user=self.request.user)


class CartListViewSet(APIView):
    serializer_class = serializers.CartListSerializer

    def get(self, request):
        queryset = models.Cart.objects.filter(ordered=False)
        queryset1 = queryset.filter(user=self.request.user)
        serializer = self.serializer_class(queryset1, many=True)
        return Response(serializer.data)


class ProductImageViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.ProductImageSerializer
    queryset = models.ProductImage.objects.all()


class ProductSearchViewSet(viewsets.ModelViewSet):
    # permission_classes = (IsAuthenticated,)
    serializer_class = serializers.ProductSearchSerializer
    queryset = models.Product.objects.all()
    filter_backends = (filters.SearchFilter, )
    search_fields = ('name', 'category', 'rating')


class ProductViewSet(viewsets.ModelViewSet):
    # permission_classes = (IsAuthenticated,)
    serializer_class = serializers.ProductSerializer
    queryset = models.Product.objects.all()
    filter_backends = (filters.SearchFilter, )
    search_fields = ('name', 'category', 'rating')

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class OrdersHistoryViewSet(APIView):

    # def post(self, request, format=None):
    #     queryset = Cart.objects.all()

    #     serializer = serializer.CartSerializer(queryset, many=True)

    #     content = {'ordered': 'true'}

    #     return Response(content)

    def get(self, request, *args, **kwargs):
        # orders = Cart.objects.all()
        # for item in orders:
        #     item.ordered = True
        #     item.save()
        # print(orders)

        queryset = Cart.objects.filter(ordered=True)
        serializer = serializers.CartSerializer(queryset, many=True)
        return Response({"Ordered list": serializer.data})


class UpdateProfile(APIView):
    serializer_class = MyUserSerializer

    def put(self, request):
        print(request.data)
        email = request.data['email']
        full_name = request.data['full_name']
        phoneNumber = request.data['phoneNumber']
        location = request.data['location']
        try:
            user = get_user_model().objects.get(email=email)
            user.email = email
            user.full_name = full_name
            user.phoneNumber = phoneNumber
            user.location = location
            user.save()
            content = {
                'email': email,
                'full_name': full_name,
                'phoneNumber': phoneNumber,
                'location': location
            }
            return Response(content, status=status.HTTP_200_OK)
        except:
            content = {'message': 'User doesnot exist'}
            return Response(content, status=status.HTTP_400_BAD_REQUEST)


class OrderList(viewsets.ModelViewSet):
    serializer_class = serializers.OrdersSerializer
    queryset = models.Order.objects.all()
    permission_classes = (IsAuthenticated, )

    def perform_create(self, serializer):

        # queryset = models.Cart.objects.filter(
        #   ordered=False, user=self.request.user)
        print(self.request.data)
        cart_item = self.request.data.getlist('cart_items')
        print(cart_item)
        # cart_item = self.request.data.getlist('queryset')
        for citem in cart_item:
            orders = Cart.get_object(citem)
            orders.ordered = True
            # orders.save(ordered=True)
            orders.save()
            print(orders)
        serializer.save(owner=self.request.user)


class OrdersHistoryViewSet1(APIView):
    serializer_class = serializers.OrdersSerializer
    queryset = models.Order.objects.all()
    permission_classes = (IsAuthenticated, )

    def get(self, request):
        queryset = Order.objects.all()
        serializer = serializers.OrdersHistorySerializer(queryset, many=True)
        return Response(serializer.data)


class CalculateDistance(APIView):
    def get(self, request):
        #serializer_class = MyUserSerializer
        queryset = get_user_model().objects.all()
        serializer = serializers.MyUserSerializer(queryset, many=True)
        for uitem in queryset:
            user = get_user_model().objects.get(email=uitem)
            print(user)
        # print(queryset.longitude)
        lat1 = radians(28.224216)
        lat2 = radians(28.222317)
        long1 = radians(83.980505)
        long2 = radians(83.993526)

        dlon = long2 - long1
        dlat = lat2 - lat1
        a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
        c = 2 * asin(sqrt(a))
        r = 6371  # Radius of earth in kilometers. Use 3956 for miles

        #query = """SELECT * from Myuser where latitude=null"""
        #serializer = Product.objects.MyUserSerializer(queryset, many=True)

        return Response(serializer.data)
