from django.conf.urls import url, include
# from rest_framework.urlpatterns import format_suffix_patterns

from . import views
from rest_framework.routers import DefaultRouter
from django.conf import settings
from django.conf.urls.static import static


router = DefaultRouter()
#router.register('Products', views.ProductsViewSet.as_view(), base_name="home")


# router.register('Orders', views.OrdersViewSet, base_name="testing")
router.register('RatingProduct', views.RatingViewSet)
# router.register('Address', views.AddressViewSet)
router.register('Cart', views.CartViewSet)
router.register('Product', views.ProductViewSet)
router.register('ProductImage', views.ProductImageViewSet)
router.register('orderlist', views.OrderList)


urlpatterns = [
    url(r'^users/me/(?P<pk>\d+)$', views.MyUserMe.as_view()),
    url(r'', include(router.urls)),
    url(r'^home/$', views.HomeViewSet.as_view()),
    url(r'^order-history/$', views.OrdersHistoryViewSet.as_view()),
    url(r'^edit-profile/$', views.UpdateProfile.as_view()),
    url(r'^cart-list/$', views.CartListViewSet.as_view()),
    url(r'^distance/$', views.CalculateDistance.as_view()),
    url(r'^order-history1/$', views.OrdersHistoryViewSet1.as_view()),
    # url(r'^order-create/$', views.OrderList.as_view()),

]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


# urlpatterns = format_suffix_patterns(urlpatterns)
