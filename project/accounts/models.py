from django.db import models
from authemail.models import EmailUserManager, EmailAbstractUser
from decimal import Decimal


class MyUser(EmailAbstractUser):
    # Custom fields
    date_of_birth = models.DateField('Date of birth', null=True,
                                     blank=True)
    prof_pic = models.ImageField(null=True, blank=True, upload_to='Profile')

    # Required
    objects = EmailUserManager()


CATEGORY_CHOICES = (
    ('BreakFast', 'BreakFast'),
    ('Lunch', 'Lunch'),
    ('Dinner', 'Dinner'),
    ('Snacks', 'Snacks'),

)


class Product(models.Model):
    category = models.CharField(choices=CATEGORY_CHOICES, max_length=30)
    name = models.CharField(max_length=50)
    price = models.DecimalField(
        max_digits=10, decimal_places=2, default=Decimal('0.00'))
    rating = models.PositiveSmallIntegerField(default=0)
    description = models.TextField(default='Product description.')
    image_url = models.ManyToManyField('ProductImage')
    Time_to_make = models.FloatField(default=False)
    owner = models.ForeignKey(
        'MyUser', related_name='products', on_delete=models.CASCADE)

    class Meta:
        db_table = 'product'

    def __str__(self):

        return self.name


RATING = (
    (1, 1),
    (2, 2),
    (3, 3),
    (4, 4),
    (5, 5)
)


class ProductsRating(models.Model):
    product_name = models.ForeignKey(
        'Product', related_name="productrating", on_delete=models.CASCADE, null=True)
    feedback = models.TextField(default='Product Feedback.')
    owner = models.ForeignKey(
        'MyUser', related_name='products_owner', on_delete=models.CASCADE)


class Address(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey('MyUser', related_name="addresses")
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    street_address1 = models.CharField(max_length=255)
    street_address2 = models.CharField(max_length=255, blank=True, null=True)
    city = models.CharField(max_length=255)
    state = models.CharField(max_length=255)
    zip = models.CharField(max_length=255)
    phone = models.CharField(max_length=255, blank=True, null=True)


class Cart(models.Model):
    # status = models.CharField(max_length=255, blank=True, null=True)
    user = models.ForeignKey('MyUser', related_name="carts")
    items = models.ForeignKey(
        'Product', related_name="product_cart", on_delete=models.CASCADE,)
    #order = models.ForeignKey("Order", blank=True, null=True)
    quantity = models.PositiveSmallIntegerField(default=0)
    ordered = models.BooleanField(default=False)

    def __str__(self):
        return str(self.items)

    def get_object(pk):
        try:
            return Cart.objects.get(pk=pk)
        except Cart.DoesNotExist:
            raise Http404


class Order(models.Model):
    date_created = models.DateTimeField(auto_now_add=True, null=True)
    full_name = models.CharField(max_length=255, null=True)
    street_address = models.CharField(max_length=255, null=True)
    city = models.CharField(max_length=255, null=True)
    state = models.CharField(max_length=255, null=True)
    zip = models.CharField(max_length=255, null=True)
    phone = models.CharField(max_length=255, blank=True, null=True)
    cart_items = models.ManyToManyField(Cart)
    owner = models.ForeignKey('MyUser', related_name="orders")

    def __str__(self):
        return str(self.cart_items)


class ProductImage(models.Model):
    image = models.ImageField(null=True, blank=True, upload_to='Product')

    def __str__(self):
        return str(self.image)
