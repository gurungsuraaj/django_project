from django.contrib import admin

# Register your models here.
from django.contrib import admin
from django.contrib.auth import get_user_model
from authemail.admin import EmailUserAdmin
from . import models


class MyUserAdmin(EmailUserAdmin):
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        ('Personal Info', {'fields': ('full_name',)}),
        ('Permissions', {'fields': ('is_active', 'is_staff',
                                    'is_superuser', 'is_verified', 'is_cook', 'phoneNumber', 'location', 'longitude', 'latitude',
                                    'groups', 'user_permissions')}),
        ('Important dates', {'fields': ('last_login', 'date_joined')}),
        ('Custom info', {'fields': ('date_of_birth', 'prof_pic')}),
    )


admin.site.unregister(get_user_model())
admin.site.register(get_user_model(), MyUserAdmin)

admin.site.register(models.Product)
admin.site.register(models.ProductsRating)
admin.site.register(models.Order)
admin.site.register(models.ProductImage)
admin.site.register(models.Cart)
