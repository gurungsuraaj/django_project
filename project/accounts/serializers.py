from django.contrib.auth import authenticate, get_user_model
from rest_framework import serializers
from rest_framework.serializers import SerializerMethodField

from . import models
from .models import ProductsRating


class MyUserSerializer(serializers.ModelSerializer):
    """
    Write your own User serializer.
    """
    class Meta:
        model = get_user_model()
        fields = ('id', 'email', 'full_name',  'date_of_birth', 'is_cook',
                  'phoneNumber', 'location', 'longitude', 'latitude')


class ProductImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.ProductImage
        fields = ('id', 'image')


class ProductSerializer(serializers.ModelSerializer):
    #owner = MyUserSerializer(required=True)

    class Meta:
        model = models.Product
        fields = ('id', 'category', 'name', 'price', 'rating', 'description',
                  'Time_to_make', 'image_url')
        depth = 1


class RatingSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.ProductsRating
        fields = ('product_name', 'feedback', 'owner')


class ProductUserSerializer(serializers.ModelSerializer):
    #owner = MyUserSerializer(required=True)
    product_rating1 = serializers.SerializerMethodField('get_rating')
    owner = MyUserSerializer(required=True)
    image_url = ProductImageSerializer(many=True)

    def get_rating(self, product):
        print("this is product", product)
        #queryset = ProductsRating.objects.filter(product_name=product)
        queryset = ProductsRating.objects.filter(product_name=product)
        serializer = RatingSerializer(instance=queryset, many=True)
        return serializer.data
        print(queryset)
        print(serializer.data)
        # print(queryset)

    class Meta:
        model = models.Product
        fields = ('id', 'category', 'name', 'price', 'rating', 'description',
                  'Time_to_make', 'image_url', 'product_rating1', 'owner')


class ProductSearchSerializer(serializers.ModelSerializer):
    #owner = MyUserSerializer(required=True)

    class Meta:
        model = models.Product
        fields = ('id', 'category', 'name', 'price', 'rating', 'description',
                  'Time_to_make', 'image_url', 'owner')
        depth = 1


# class AddressSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = models.Address
#         fields = '__all__'


class CartSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()

    class Meta:
        model = models.Cart
        fields = ('id', 'items', 'quantity', 'ordered')


class CartListSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()
    items = ProductSearchSerializer(required=True)

    class Meta:
        model = models.Cart
        fields = ('id', 'items', 'quantity', 'ordered', 'user')


class OrdersSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()

    class Meta:
        model = models.Order
        fields = ('id', 'date_created', 'full_name', 'street_address',
                  'city', 'state', 'zip', 'phone', 'cart_items')


class OrdersHistorySerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()

    class Meta:
        model = models.Order
        fields = ('id', 'date_created', 'full_name', 'street_address',
                  'city', 'state', 'zip', 'phone', 'cart_items')
        depth = 2


class HomeSerializer(serializers.ModelSerializer):
    # owner = SerializerMethodField()
    owner = MyUserSerializer(required=True)
    image_url = ProductImageSerializer(many=True)

    class Meta:
        model = models.Product
        fields = ('id', 'category', 'name', 'price', 'rating',
                  'description', 'image_url', 'Time_to_make', 'owner')

    # def get_owner(self,obj):
    #     return (obj.owner.email)
